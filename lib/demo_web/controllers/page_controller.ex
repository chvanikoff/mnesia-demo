defmodule DemoWeb.KVController do
  use DemoWeb, :controller

  alias Demo.Repo.Mnesia
  alias Demo.KV

  def get(conn, %{"key" => key}) do
    val = case Mnesia.get_by(KV, key: key) do
      nil ->
        nil
      %{value: value} ->
        value
    end
    json(conn, %{val: val})
  end

  def put(conn, %{"key" => _key, "value" => _value} = data) do
    cs = KV.changeset(data)
    status = case cs.valid? do
      true ->
        Mnesia.insert!(cs)
        "success"
      false ->
        "error"
    end
    json(conn, %{status: status})
  end
end
