FROM bitwalker/alpine-elixir-phoenix:1.5.0

RUN apk --no-cache upgrade \
  # Distillery requires bash
  && apk add --no-cache bash

ENV MIX_ENV=prod \
    REPLACE_OS_VARS=true

RUN mkdir build
WORKDIR build

COPY mix.exs mix.lock ./
RUN mix do deps.get, deps.compile

COPY . ./

RUN mix compile

ARG VERSION

RUN mix release --env=prod \
  && cp _build/prod/rel/demo/releases/${VERSION}/demo.tar.gz ../

WORKDIR ../

RUN tar -xf demo.tar.gz \
  && rm -rf build demo.tar.gz

USER default

ENTRYPOINT ["/opt/app/bin/demo"]
CMD ["foreground"]
