defmodule DemoWeb.Router do
  use DemoWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", DemoWeb do
    pipe_through :api

    get "/get/:key", KVController, :get
    get "/put/:key/:value", KVController, :put
  end
end
