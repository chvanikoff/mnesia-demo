use Mix.Config

config :demo, DemoWeb.Endpoint,
  load_from_system_env: true,
  url: [host: "example.com", port: 4001],
  http: [:inet6, port: 4001],
  cache_static_manifest: "priv/static/cache_manifest.json",
  secret_key_base: "${PHX_SECRET_KEY_BASE}",
  server: true

config :demo,
  env: :prod

config :logger,
  level: :warn,
  backends: [
    :console,
  ]

config :libcluster,
  topologies: [
    k8s: [
      strategy: Cluster.Strategy.Kubernetes,
      config: [
        kubernetes_selector: "app=demo,tier=backend",
        kubernetes_node_basename: "demo"
      ]
    ]
  ]
