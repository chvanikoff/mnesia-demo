defmodule Demo.Repo.Mnesia.Migrations.CreateKv do
  use Ecto.Migration

  def change do
    create_if_not_exists table(:kv, engine: :set) do
      add :key, :string
      add :value, :string
    end

    create index(:kv, [:key])
  end
end
