defmodule Demo.KV do
  use Ecto.Schema

  import Ecto.Changeset

  @derive {Poison.Encoder, except: [:__meta__]}
  @type t :: %__MODULE__{}

  schema "kv" do
    field :key, :string
    field :value, :string
  end

  def changeset(attrs), do: changeset(%__MODULE__{}, attrs)

  def changeset(%__MODULE__{} = application, attrs) do
    application
    |> cast(attrs, [:key, :value])
    |> validate_required([:key, :value])
    |> unique_constraint(:key)
  end
end
