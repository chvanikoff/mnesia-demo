# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# Configures the endpoint
config :demo, DemoWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "wt1BlaFPpiRTk/PKrdK/Y8MqchTV9ypxzYAl/yR7oabw3yqEsgvtAA2x54Dxp+mR",
  render_errors: [view: DemoWeb.ErrorView, accepts: ~w(json)],
  pubsub: [name: Demo.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

config :demo,
  ecto_repos: [Demo.Repo.Mnesia]

config :demo, Demo.Repo.Mnesia,
  adapter: EctoMnesia.Adapter,
  host: :"demo@${NODE_IP}"

config :ecto_mnesia,
  host: :"demo@${NODE_IP}",
  storage_type: :disk_copies

config :mnesia,
  dir: 'mnesia-data'

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
