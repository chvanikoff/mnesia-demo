defmodule Demo.Repo.Mnesia do
  use Ecto.Repo, otp_app: :demo

  def init(_, opts) do
    {:ok, opts}
  end
end
