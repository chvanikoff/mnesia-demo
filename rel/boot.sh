#!/bin/sh
set -e

export NODE_IP=$(wget -qO- http://rancher-metadata.rancher.internal/latest/self/container/primary_ip)

/opt/app/bin/demo $@
